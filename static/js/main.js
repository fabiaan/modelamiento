document.getElementById("link-one").addEventListener("mouseover", mouseOverOne);
document.getElementById("link-one").addEventListener("mouseout", mouseOutOne);

function mouseOverOne() {
    document.getElementById("icon-one").style.color = "#4290F5";
}
function mouseOutOne() {
    document.getElementById("icon-one").style.color = "#667182c7";
}

document.getElementById("link-two").addEventListener("mouseover", mouseOverTwo);
document.getElementById("link-two").addEventListener("mouseout", mouseOutTwo);

function mouseOverTwo() {
    document.getElementById("icon-two").style.color = "#4290F5";
}
function mouseOutTwo() {
    document.getElementById("icon-two").style.color = "#667182c7";
}

