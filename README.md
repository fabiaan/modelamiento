# ULEAM

Universidad Laica Eloy Alfaro de Manabí

Facultad de Ciencias Informáticas

Carrera Tecnologías de la Información


**Nombre:**

Pablo Fabian Proaño Medina

**Curso:**

Sexto "A"

**Asignatura:**

Modelamiento y Simulación

**Tema:**

Proyecto final y Software

**Docente:**

Ing. Jorge Anibal Moya Delgado

**Software:**

[Aplicación web](https://app-modelos.herokuapp.com/)


Manta - Manabí - Ecuador

2021-1